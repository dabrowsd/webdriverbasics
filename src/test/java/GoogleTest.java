import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class GoogleTest {

    private WebDriver driver;

    @Before
    public void onInitialize(){
        driver = new FirefoxDriver();
        System.setProperty("webdriver.gecko.driver", "C:\\geckodriver");
    }

    @After
    public void closeTests(){
        driver.close();
    }

    @Test
    public void openGooglePageTest(){
        driver.get("http://www.google.com");
        WebElement element = driver.findElement(By.id("gs_lc0"));

        element.sendKeys("Cheese!");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        element.submit();

        Assert.assertTrue(driver.getTitle().equals("Google"));
    }

    @Test
    public void countResultsOnPage(){
        JavascriptExecutor js = (JavascriptExecutor) driver;

        driver.get("http://www.google.com");
        WebElement element = driver.findElement(By.id("gs_lc0"));

        element.sendKeys("Weird Owl");

        element.submit();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("logo")));

        long links = (Long) js.executeScript
                ("var links = document.getElementsByTagName ('A'); return links.length");

        System.out.println(links);
        Assert.assertTrue(links == 33);
    }

}
